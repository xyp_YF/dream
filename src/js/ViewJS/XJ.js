/**
 * Created by xyp on 2017/5/24.
 */
var XJ=function () {

    var self=this;

    self.data={};
    self.app={};
    self.listType="";

    self.init=function () {

    }

    self.bind=function (id) {
        console.log(id);
        self.data=$("#"+id);
        self.app = new Vue({
            el: '#'+id,
            data: {
                list: []
            }
        })
    }

    self.init.asTextList=function () {
            Vue.component('text-list', {
                props: ['text'],
                template: '<li style="list-style-type: none">{{ text.text }}</li>'
            })
            self.listType="text-list";
        }



    //     <ul class="ui-list ui-list-pure ui-border-tb">
    //     <li class="ui-border-t">
    //     <p><span>1.faycheng </span><span class="date"> 2月12日</span></p>
    // <h4>这本书太赞了，每次看都有不一样的体会和感悟，超级喜欢！期待大结局。</h4>
    // </li>
    //
    // </ul>
    self.init.asCommentList=function () {
        Vue.component('comment-list', {
            props: ['comment'],
            template: '<ul class="ui-list ui-list-pure ui-border-tb">'+
                '<li class="ui-border-t">'+
                '<p><span>{{comment.author}}</span><span class="date"> {{comment.date}}</span></p>'+
                '<h4>{{comment.content}}</h4>'+
                '</li></ul>'
        })
        self.listType="text-list";
    }

    self.init.asImgList=function () {
        Vue.component('text-list', {
            props: ['text'],
            template: '<ul><li style="list-style-type: none">{{ text.title }}</li>' +
            '<li style="list-style-type: none">{{ text.name }}</li></ul>'
        })
        self.listType="text-list";
    }

    self.bind.data=function (data) {
        self.app.list=data;
    }

    self.bind.url=function (url) {
        $.get(url,function (data) {
            self.app.list=data;
        })
    }
}

